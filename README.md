## Estructuras de control en Ruby

Ruby tiene las siguientes estructuras de control:

- Estructuras de control condicionales: `if`, `else`, `elsif`, `unless` y `case`.
- Estructuras de control iterativas: `while`, `until loop`, `for`, `loop`, `each`, `times`.


## Estructuras de control condicionales

Alteran el funcionamiento de un procedimiento de acuerdo a una condición.

#### Estructuras de control `if`, `else`, `elsif` 

Estas estructuras revisan que una condición se cumpla para ejecutar una pieza de código.

```Ruby
if <conditional>:
  #piece of code
elif <conditional>
  #piece of code
else
  #piece of code
end
```

```ruby
a = 5
b = 6
if a == b
  puts "a y b son iguales"
else
  puts "a y b son diferentes"
end
```

#### Estructura de control `case`

La estructura `case` revisa que las condiciones se cumplan para ejecutar una parte del código. Es bastante útil cuando se tienen varias opciones de condiciones.

Esta estrutura se usa cuando se tienen varias opciones de condiciones. Una vez que se cumpla una condición se ejecuta una parte del código.


```ruby
speed = 50
case speed
when 0..20
  puts "You are One"
when 21..30
  puts "You are Second"
when 31..40
  puts "You are Third"
else
  puts "You are the best"
end
```

#### Estructura de control `unless` 

Esta estructura de control sirve para verificar que la condición sea falsa. Es igual a decir `if not`.


```ruby
greet = "Hi"
puts "El saludos es Hi" unless greet == "Hello"
```
```Ruby
greet = "Hi"
unless greet == "Hello" 
   puts "El saludo es Hi"
else
   puts "El saludo es Hello"
end
```

## Estructuras de control iterativas

Esta estructura permite ejecutar un bloque de código una cantidad específica de veces.

#### Estructura de control `for` 

Esta estructura se usa cuando se conoce las veces a iterar y se ejecuta una vez para cada elemento.

```ruby
for value in 5..10
  puts "El valor de value es: #{value}"
end
``` 

#### Estructura de control `each`

Esta estructura en Ruby se usa cuando queremos iterar sobre colecciones como un arreglo.


```ruby
array = [1,2,3,4,5]

array.each do |number|
  p "#{number}" if number % 2 == 0
end
```

#### Estructura de control `while`

Esta estructura se usa cuando no sabes a que punto llegar y se conoce la condición, se ejecuta mientras la condición es verdadera.


```ruby
floor = 1

while floor <= 10
  puts "El piso es: #{floor}"
  #este contador se usa para evitar loop infinito
  floor += 1
end
```

#### Estructura de control `until` 

Esta estructura se ejecuta hasta que la condición sea falsa.

```ruby
floor = 1
until floor > 10 
  puts "El piso es #{floor}"
  floor += 1
end
```

#### Estructura de control `loop` 

Esta estructura iterativa requiere una condición para romper la iteración.

```ruby
floor = 20
loop do
  p "#{floor}"
  floor -= 1
  #condición necesaria para romper el loop
  break if floor <= 0
end
```

Si usamos `next` es posible saltar ciertos pasos en el loop:

```ruby
floor = 21
loop do
  floor -= 1
  #se usa next para saltar si número es par.
  next if floor % 2 == 0
  #con el uso de next se imprimen sólo números impares.
  p "#{floor}"
  break if floor <= 1
end
```

#### Estructura de control `times` 

Esta estructura permite que que una tarea sea desarrollada un número determinado de veces.

```ruby
number = 5

number.times do
  p "Hola"
end
```

## Ejercicio - Identifica prime en arreglo

Para este ejercicio es importante documentarse acerca de los números primos y como en Ruby se pueden insertar valores en un arreglo.

Define el método `prime` que evaluará si un número en un arreglo dado es primo. El resultado de las pruebas `driver code` deben ser `true`.

El siguiente pseudocódigo puede ser utilizado para saber si un número es primo:

- 1) Definir un Número 'x'.
- 2) Definir un Rango (progresión aritmética) de 2 a 'x' - 1.
- 3) Evaluar, Si el residuo de la división de 'x' entre algún número del Rango es cero entonces 'x' no es primo.

```ruby
#prime method


#driver code
p prime([2, 3, 4, 5, 6, 7, 8, 9, 10]) == ["prime", "prime", 4, "prime", 6, "prime", 8, 9, 10]
p prime([23, 54, 7, 123, 56, 76, 83, 101]) == ["prime", 54, "prime", 123, 56, 76, "prime", "prime"]

``` 

##Ejercicio - Identifica prime con método nativo de Ruby


Define el método `is_prime?` que evaluará si un número en un arreglo dado es primo con la ayuda de un método nativo de ruby. El resultado de las pruebas `driver code` deben ser `true`.

Para este ejercicio hay que considerar lo siguiente:

- Es importante documentarse acerca del método nativo de ruby que permite identificar un número primo. 
- No está permitido definir otro arreglo en el método, hay que retornar el mismo arreglo con el elemento correspondiente, ya sea el string `"prime"` o el número evaluado. Hay que documentarse acerca de como ruby puede realizar esta operación a través del uso de métodos enumerables.
- Solamente es posible usar el operador ternario en cualquier estructura condicional.

```ruby
#is_prime? method


#driver code
p is_prime?([2, 3, 4, 5, 6, 7, 8, 9, 10]) == ["prime", "prime", 4, "prime", 6, "prime", 8, 9, 10]
p is_prime?([23, 54, 7, 123, 56, 76, 83, 101]) == ["prime", 54, "prime", 123, 56, 76, "prime", "prime"]

``` 

Tips:

- Una referencia con el uso de enumerables en ruby es la siguiente: [Enumerables](http://ruby.bastardsbook.com/chapters/enumerables/). 

